#!/bin/bash

case $1 in
    start) services="name_server topic_server" ;;
    stop) services="topic_server name_server" ;;
    *) echo "Command must be 'start' or 'stop'"; exit ;
esac

for s in $services ; do
    sudo systemctl $1 pyro_$s
    sleep 1
    sudo systemctl status -n 20 -l pyro_$s
done
