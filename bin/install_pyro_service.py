#!/usr/bin/python3
# coding=utf-8

"""Installer for setting up the Pyro4 objects as system services. This
must be called in the the directory where the .service-file is
located. It is assumed that the format of the service file is "pyro_{service_name}.service"

"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF 2014"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"

import argparse
import subprocess
import os
import sys

arg_parser = argparse.ArgumentParser()
arg_parser.add_argument('service_name', type=str,
                        help="""Service name, without 'pyro_'-prefix and '.service'-suffix."""
                    )
arg_parser.add_argument('--exec_file', type=str, default=None,
                        help="""The executable file target for this service. It may be given as
                        relative to current working directory or as an
                        absolute path. If not given, one will be
                        sought in the local pyro_dpcs config file for
                        the service."""
                    )
arg_parser.add_argument('-u', '--user', type=str, default=os.getlogin(),
                        help="""The user name under which this service should run."""
                    )
arg_parser.add_argument('--enable', action='store_const', dest='enable', default=False, const=True)
arg_parser.add_argument('--start', action='store_const', dest='start', default=False, const=True)
args, _ = arg_parser.parse_known_args()


svcname = '{}.service'.format(args.service_name)
svcconffile = '/home/{}/.pyro_dpcs/{}.conf'.format(args.user, args.service_name)
# Seek for exec_file in service config.
if args.exec_file is None:
    for conf in open(svcconffile):
        var,key = conf.strip().split('=')
        if var == 'exec_file':
            print('Found executable file target in config file: {}'.format(key))
            args.exec_file = key
else:
    # Ensure absolute path.
    args.exec_file = os.path.abspath(args.exec_file)
    
svcnametmpl = svcname + '.template'
if os.path.isfile(svcnametmpl):
    # Parse and dump in svcname-file
    template = open(svcnametmpl).read()
    svcfile = open(svcname, 'w')
    svcfile.write(template.format(**args.__dict__))
    svcfile.close()
   
svcpath = os.path.abspath(svcname)
svcinstname = 'pyro_'+svcname
svcinstpath = os.path.join('/etc/systemd/system/', svcinstname)
#svcconfname = 'pyro_topic_server.service'
#svcconfinstpath = os.path.join('~/.pyro_dpcs', svcconfname)

# Check if the service file exists locally
if not os.path.isfile(svcname):
    print('Error: The local service file "{}" for the specified service does not exist!'.format(svcname))

if not os.path.isfile(svcinstpath) or not os.path.samefile(svcpath, svcinstpath):
    print('Installing "{}" in systemd'.format(svcinstname))
    subprocess.call('sudo ln -f {} {}'.format(svcpath, svcinstpath), shell=True)
if args.enable:    
    print('Enabling "{}"'.format(svcname))
    subprocess.call('sudo systemctl enable {}'.format(svcinstname), shell=True)
    subprocess.call('sudo systemctl daemon-reload', shell=True)
else:
    print('Disabling "{}"'.format(svcname))
    subprocess.call('sudo systemctl disable {}'.format(svcinstname), shell=True)

if args.start:
    print('Starting "{}"'.format(svcname))
    subprocess.call('sudo systemctl restart {}'.format(svcinstname), shell=True)
