# coding=utf-8

"""
Module for providing the basic communication and naming facilities
of a HMS- or agent-based communication system over Pyro. This
package-module contains the necessary code for connecting to a name-
and topic server for active objects.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF 2014-2018"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"

import time
import os
import atexit
import threading
import argparse
import logging
import platform

from .config import Pyro4

from .typed_name_server import TypedNameServer

try:
    import netifaces

    # Collect possible AF_INET addresse
    addr_list = []
    for nif in netifaces.interfaces():
        nifaddrs = netifaces.ifaddresses(nif)
        if netifaces.AF_INET in nifaddrs:
            for nifinet in nifaddrs[netifaces.AF_INET]:
                addr_list.append(nifinet['addr'])
    addr_list.append(platform.node())
    addr_list.append('local')
    addr_list.append('first_local')
except:
    addr_list = None

_arg_parser = None


# Function to interpret a string to bool
def _sbool(s):
    return s.lower().strip() in ['true', '1', 't', 'ok']


def add_arguments(arg_parser=None):
    global _arg_parser
    if arg_parser is None:
        _arg_parser = argparse.ArgumentParser('Pyro4 Utils')
    else:
        _arg_parser = arg_parser
    pu_group = _arg_parser.add_argument_group('Pyro4 Utils')

    pu_group.add_argument(
        '--log_level', type=str,
        choices=list(logging._nameToLevel.keys()), default='INFO',
        help="""The default log level to use throughout the process. Individual
                          loggers may set their own log level.""")

    pu_group.add_argument(
        '--log_file', type=str, default='',
        help="""If set to something different than the default '', the logging will
        be sent to that named file. """)

    pu_group.add_argument(
        '--daemon_host', type=str, choices=addr_list, default=platform.node(),
        help="""The host to bind newly created, Pyro-exposed objects by the
        daemon. This must be the address on the network on which the
        objects registered in this process can be contacted. If not
        given explicitly, the first non-local address is used;
        i.e. one different from '127.0.0.1'.""")

    pu_group.add_argument(
        '--name_server_host', type=str, default=None,
        help="""The address for which to search for the Pyro4 name server. If left
        to default the name server is searched at the local host.""")

    pu_group.add_argument(
        '--topic_server_name', type=str, default='TopicServer',
        help="""The name of the Pyro Topic Server. Only set if using an
        off-standard name, e.g. when using several topic servers.""")

    pu_group.add_argument(
        '--use_topic_server', type=_sbool, default=True,
        help="""Option to select if a topic server connection should be
        established.""")

    pu_group.add_argument(
        '--host', type=str, default='127.0.0.1',
        help="""The default host ip or name to use for all options requiring an
        address. If '--name_server_host' is not set, it will get this
        host address.""")

    pu_group.add_argument(
        '--init_timeout', type=int, default=600,
        help="""The time allowed for retrying connecting to the name and topic
        servers. Retries will be performed at 1 second intervals until
        either a connection has been established, or the init time has
        been spent.""")

# args, unknown_args = arg_parser.parse_known_args()

pyro_name_server = None
name_server = None
topic_server = None

_log = logging.getLogger('Pyro4Utils')

_args = None


def init(args=None):
    global _log, pyro_name_server, name_server, topic_server, \
        _release, daemon, _args, _arg_parser
    if _arg_parser is None:
        add_arguments()
    if args is None:
        _args, _uargs = _arg_parser.parse_known_args()
    elif type(args) == list:
        _log.info('Getting arguments from a list')
        _args, _uargs = _arg_parser.parse_known_args(args)
    elif type(args) == argparse.Namespace:
        _args = args
    else:
        raise Exception('Args given to pyro4_utils.init must be None, '
                        + 'a list, or an argparse.Namespace. Got "{}"'
                        .format(type(args)))
    _log.setLevel(_args.log_level)
    _log.debug('Initializing on the following arguments : {}'.format(_args))
    _log.info('Log file argument "{}"'.format(_args.log_file))
    if not _args.log_file == '':
        log_file = os.path.expandvars(_args.log_file)
        _log.info('Using log file "{}"'.format(log_file))
        fh = logging.FileHandler(log_file)
        fh.setFormatter(logging.Formatter(
            '%(asctime)s:%(levelname)s:%(name)s : %(message)s'))
        logging.root.addHandler(fh)

    # Check and set host names or IPs
    if _args.name_server_host is None:
        _args.name_server_host = _args.host
    if _args.daemon_host is None:
        _args.daemon_host = _args.host
    elif _args.daemon_host == 'first_nonlocal':
        non_loc_addr = [a for a in addr_list if a != '127.0.0.1']
        _args.daemon_host = non_loc_addr[0]
    elif _args.daemon_host == 'local':
        _args.daemon_host = '127.0.0.1'

    _log.info('Starting Pyro4 daemon for registering objects on host "{}".'
              .format(_args.daemon_host))
    daemon = Pyro4.Daemon(host=_args.daemon_host)
    _daemon_thread = threading.Thread(target=daemon.requestLoop)
    _daemon_thread.daemon = True
    _daemon_thread.start()

    init_limit = time.time() + int(_args.init_timeout)
    while True:
        try:
            _log.info('Trying name server at address "{}".'
                      .format(_args.name_server_host))
            pyro_name_server = Pyro4.locateNS(host=_args.name_server_host)
        except:
            _log.warn('Name server not currently available.'
                      + ' Retrying in 1 second.')
        else:
            _log.info('Raw name server connection established. Creating typing decorator.')
            name_server = TypedNameServer(pyro_name_server, daemon)

            break
        if time.time() > init_limit:
            _log.error('A name server connection could not be'
                       + ' estblished within the given timeout!')
            break
        time.sleep(1)
    if name_server is not None and _args.use_topic_server:
        _log.info('Getting Topic Server from name server name "{}".'
                  .format(_args.topic_server_name))
        while True:
            if _args.topic_server_name in name_server.list():
                topic_server = Pyro4.Proxy(
                    name_server.lookup(_args.topic_server_name))
                _log.info('Topic server connection established.')
                break
            else:
                _log.warn(('The topic Server is currently not registered '
                          + 'in the name server. Retrying in 1 second.'))
            if time.time() > init_limit:
                _log.warn('A topic server connection could not '
                          + 'be estblished within the given timeout!')
                break
            time.sleep(1)

    _log.info('Pyro4 Utils Ready.')

    def _shutdown():
        if pyro_name_server is not None:
            _log.info('Releasing name server proxy.')
            pyro_name_server._pyroRelease()
        if topic_server is not None:
            _log.info('Releasing topic server proxy.')
            topic_server._pyroRelease()
        _log.info('Shutting down daemon.')
        daemon.shutdown()

    atexit.register(_shutdown)


def conf_init(utils_conf_file=os.path.expandvars('$HOME/.pyro_dpcs/utils.conf')):
    """Initialize Pyro4 Utils on configuration file."""
    ucf = utils_conf_file
    if os.path.isfile(ucf):
        _log.info('Opening config file for setup.')
        arglist = open(ucf).read().strip().split('\n')
        pu_args = []
        for arg in arglist:
            if arg == '' or arg[0] == '#':
                continue
            else:
                pu_args.append('--' + arg)
        _log.info('Using following config from file: {}'.format(pu_args))
        init(pu_args)
    else:
        _log.info('Using default setup of pyro4_utils.')
        init()
